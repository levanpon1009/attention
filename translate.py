# Imports the Google Cloud client library
from google.cloud import translate

# Instantiates a client
translate_client = translate.Client()

# The text to translate
text = u'Hello, world!'
# The target language
target = 'vi'

import unicodedata
import re
import numpy as np
import os
import io
import time



# Converts the unicode file to ascii
def unicode_to_ascii(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')


def preprocess_sentence(w):
 
    # creating a space between a word and the punctuation following it
    # eg: "he is a boy." => "he is a boy ."
    # Reference:- https://stackoverflow.com/questions/3645931/python-padding-punctuation-with-white-spaces-keeping-punctuation
 

    # adding a start and an end token to the sentence
    # so that the model know when to start and stop predicting.
    
    return w

def create_dataset(path, num_examples):
    lines = io.open(path, encoding='UTF-8').read().strip().split('\n')

    word_pairs = [[preprocess_sentence(w) for w in l.split('\t')]  for l in lines[:num_examples]]

    return zip(*word_pairs)

textFile = open('eng-vi1.txt','w',  encoding='utf-8')
en, sp = create_dataset('data/spa-eng/spa.txt', None)
# batch_size = 100
# _en = []
# vi = []
# for i in range(len(en)):
#     if i % batch_size == 0 and i != 0:
#         translations = translator.translate(_en, dest="vi")
#         for translation in translations:
#             vi.append(translation.text)
#         for j in range(len(vi)):
#             print(_en)
#             line = _en[j] + '\t' + vi[i] + '\n'
#             textFile.write(line)
#         _en = []
#     else :
#         _en.append(en[i])
count = 0
for i in range(len(en)):
    count += 1
    text = translate_client.translate(en[i], target_language=target)
    print(count)
    textFile.write(en[i] + '\t' + text['translatedText'] + '\n')
# translations = translator.translate(sp, dest='ja')
# for translation in translations:
#     vi.append(translation.text)
# print(vi[1])



# # Translates some text into Russian
# translation = translate_client.translate(
#     text,
#     target_language=target)

# print(u'Text: {}'.format(text))
# print(u'Translation: {}'.format(translation['translatedText']))